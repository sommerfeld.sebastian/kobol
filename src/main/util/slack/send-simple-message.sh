#!/bin/bash
# @file send-simple-message.sh
# @brief Send message to slack channel from terminal.
#
# @description The script sends a message to a slack channel from a bash script or terminal. Messages are sent
# to the slack channel _workstations_ of workspace _kobol_. On my laptop the ``.bashrc`` file of my user
# contains the alias ``slack-send-simple-msg``.
#
# ==== Arguments
#
# * *$1* (String): The message text (mandatory)

text=$1

if [ -z "$text" ]
then
  echo -e "$LOG_ERROR Cannot send slack message - Mandatory param 'messageText' missing"
else
  echo -e "$LOG_INFO Send simple Message to Slack channel 'workstations' of workspace 'kobol'"

  curl --location --request POST "https://hooks.slack.com/services/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" \
  --header "Content-type:  application/json" \
  --data-raw "{
      'text': '$1',
      'blocks': [
        {
          'type': 'section',
          'text': {
            'type': 'mrkdwn',
            'text': '*$1*'
          }
        }
      ]
  }"

  echo
  echo -e "$LOG_DONE Sent simple slack message"
fi
