#!/bin/bash
# @file install-node.sh
# @brief Install NodeJS.
#
# @description The script installs link:https://nodejs.org/en[NodeJS] using link:https://github.com/nvm-sh/nvm[nvm (Node Version Manager)].
#
# ==== Arguments
#
# The script does not accept any parameters.

# shellcheck disable=SC1090
# shellcheck disable=SC1091
# https://github.com/koalaman/shellcheck/wiki/SC1090
# https://github.com/koalaman/shellcheck/wiki/SC1091

echo -e "$LOG_INFO Install node via nvm (Node Version Manager)"
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash > /dev/null

echo -e "$LOG_INFO Install node"
source "$HOME/.nvm/nvm.sh"
source "$HOME/.bashrc"
nvm --version
nvm install node
nvm use node

echo -e "$LOG_INFO Install npm"
source "$HOME/.nvm/nvm.sh"
source "$HOME/.bashrc"
npm install -g npm@latest
npm --version

echo -e "$LOG_DONE Installed node"
