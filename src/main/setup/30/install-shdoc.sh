#!/bin/bash
# @file install-shdoc.sh
# @brief Install shdoc, gawk and pandoc.
#
# @description The script installs link:https://github.com/reconquest/shdoc[shdoc] (Markdown docs generator for bash
# scripts), gawk (dependency) and Pandoc (Markdown to Asciidoc converter).
#
# ==== Arguments
#
# The script does not accept any parameters.


echo -e "$LOG_INFO Install shdoc and dependencies"

sudo apt-get install -y gawk
sudo mkdir -p /opt/shdoc
(
  cd /opt/shdoc || exit
  sudo git clone --recursive https://github.com/reconquest/shdoc
  cd shdoc || exit
  sudo make install
)

echo -e "$LOG_INFO Install pandoc"
sudo apt-get install -y pandoc

echo -e "$LOG_DONE Installed shdoc"
