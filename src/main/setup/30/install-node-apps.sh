#!/bin/bash
# @file install-node-apps.sh
# @brief Install NodeJS applications.
#
# @description The script installs applications for link:https://nodejs.org/en[NodeJS].
#
# ==== Arguments
#
# The script does not accept any parameters.


echo -e "$LOG_INFO Install node apps"

npm install --global yarn
npm install --global @stencil/core@latest --save-exact
npm install --global @stencil/sass --save-dev
npm install --global webserver
npm install --global gulp-cli
npm install --global @antora/cli@2.2 @antora/site-generator-default@2.2
npm install --global asciidoctor
npm install --global @asciidoctor/core asciidoctor-pdf
npm install --global asciidoctor @asciidoctor/reveal.js
npm install --global asciidoctor-kroki
npm install --global folderslint
npm install --global @bitwarden/cli

echo -e "$LOG_DONE Installed node apps"
