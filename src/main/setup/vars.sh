#!/bin/bash
# @file vars.sh
# @brief Export variables for install scripts.
#
# @description Export variables for install scripts. The script should not be executable.
#
# NOTE: Don't run this script directly!

export LOG_DONE="[\e[32mDONE\e[0m]"
export LOG_ERROR="[\e[1;31mERROR\e[0m]"
export LOG_INFO="[\e[34mINFO\e[0m]"
export LOG_WARN="[\e[93mWARN\e[0m]"
