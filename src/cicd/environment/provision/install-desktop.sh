#!/bin/bash
# @file install-desktop.sh
# @brief Provisioning script for the Vagrantbox (= the test environment).
#
# @description The script installs the desktop environment to make sure this Vagrantbox is not headless.
#
# IMPORTANT: DON'T RUN THIS SCRIPT DIRECTLY - Script is invoked by Vagrant.
#
# ==== Arguments
#
# The script does not accept any parameters.

export Y="\e[33m" # text yellow
export D="\e[32m" # text default (vagrant green)

# Desktop environment
echo "[INFO] Start desktop installation"

sudo apt-get -y update
sudo apt-get -y upgrade
echo "[DONE] update + upgrade"

sudo apt-get install -y ubuntu-desktop
echo "[DONE] Full ubuntu-desktop installed"
