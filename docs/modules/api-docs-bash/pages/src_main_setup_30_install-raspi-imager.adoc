= install-raspi-imager.sh
Jenkins Pipeline <sebastian@sommerfeld.io>
:page-toclevels: 1

Install Raspi Imager.

== Overview

The script installs Raspi Imager.

=== Arguments

The script does not accept any parameters.
