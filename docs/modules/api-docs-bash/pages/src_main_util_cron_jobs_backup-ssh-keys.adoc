= backup-ssh.keys.sh
Jenkins Pipeline <sebastian@sommerfeld.io>
:page-toclevels: 1

Backup ssh keys to USB drive

== Overview

The script copies a bunch of ssh keys to a certain USB device. It is invoked regularly by cron.

=== Arguments

The script does not accept any parameters.
