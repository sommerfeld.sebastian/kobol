= start.sh
Jenkins Pipeline <sebastian@sommerfeld.io>
:page-toclevels: 1

Stop all exporters for prometheus.

== Overview

The script stops all exporters for prometheus.

=== Arguments

The script does not accept any parameters.
