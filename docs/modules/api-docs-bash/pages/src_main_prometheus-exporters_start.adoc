= start.sh
Jenkins Pipeline <sebastian@sommerfeld.io>
:page-toclevels: 1

Start all exporters for prometheus.

== Overview

The script starts all exporters for prometheus (see link:http://prometheus:3000[http://prometheus:3000]). The script starts one link:https://github.com/prometheus/node_exporter[node-exporter] and one link:https://github.com/google/cadvisor[cAdvisor]. All exporters are managed by docker-compose. Containers are started in the background.

=== Arguments

The script does not accept any parameters.
