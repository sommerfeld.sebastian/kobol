= set-cronjobs.sh
Jenkins Pipeline <sebastian@sommerfeld.io>
:page-toclevels: 1

Set cronjobs for current user.

== Overview

The script configures a bunch of listed cronjobs for the crontab of the current user.

* xref:api-docs-bash:src_main_util_cron_jobs_backup-ssh-keys.adoc[]
* xref:api-docs-bash:src_main_util_cleanup-filesystem.adoc[

=== Arguments

The script does not accept any parameters.
