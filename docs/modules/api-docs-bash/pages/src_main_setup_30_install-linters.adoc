= install-linters.sh
Jenkins Pipeline <sebastian@sommerfeld.io>
:page-toclevels: 1

Install linters.

== Overview

The script installs linters to check on code quality etc.

* shellcheck
* yamllint

=== Arguments

The script does not accept any parameters.
