= install-vagrant.sh
Jenkins Pipeline <sebastian@sommerfeld.io>
:page-toclevels: 1

Install Vagrant with plugins and Packer.

== Overview

The script installs link:https://www.vagrantup.com[Vagrant] with plugins and link:https://www.packer.io[Packer].

* Plugin: link:https://github.com/fgrehm/vagrant-cachier[vagrant-cachier]
* Plugin: link:https://github.com/dotless-de/vagrant-vbguest[vagrant-vbguest]

Packer is used to create identical machine images for multiple platforms from single source configuration.

=== Arguments

The script does not accept any parameters.
